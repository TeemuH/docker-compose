const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.listen(port, () => {

});

app.get('/', (req, res) => {
  const service1_remote_address = req.body.remoteAddress;
  const service1_remote_port = req.body.remotePort;
  const service1_local_address = req.body.localAddress;
  const service1_local_port = req.body.localPort;

  const service2_remote_ddress = req.client.remoteAddress;
  const service2_remote_port = req.client.remotePort;
  const service2_local_address = req.client.localAddress;
  const service2_local_port = req.client.localPort;

  res.writeHead(200, { 'Content-Type': 'text/html' });

  res.write("Hello from " + service1_remote_address + ":" + service1_remote_port + '\n');
  res.write("to " + service1_local_address + ":" + service1_local_port + '\n');
  res.write("Hello from " + service2_remote_ddress + ":" + service2_remote_port + '\n');
  res.write("to " + service2_local_address + ":" + service2_local_port);

  res.end();
});