const express = require('express');
const sendRequest = require("send-request");
const bodyParser = require('body-parser');
const app = express();
const port = 5000;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.get('/', (req, res) => {

  (async () => {
    const response = await sendRequest("http://service2:3000", {
      method: "GET",
      body: {
        remoteAddress: req.client.remoteAddress,
        remotePort: req.client.remotePort,
        localAddress: req.client.localAddress,
        localPort: req.client.localPort,
      }
    });

    res.send(response.body);
  
  })()
});

app.listen(port, () => {
  
});